import Typography from 'typography';

const typography = new Typography({
  baseFontSize: '16px',
  bodyColor: '#000',
  headerColor: '#000',
});

export default typography;
